import { EleventyI18nPlugin } from '@11ty/eleventy';
import { eleventyImageTransformPlugin } from '@11ty/eleventy-img';

export default function (eleventyConfig) {
  // Serve CSS and assets along with the site
  eleventyConfig.addPassthroughCopy({'published/css/*.css': 'css'});
  eleventyConfig.addPassthroughCopy({'published/assets/': 'assets'});

  // For the fetch plugin
  eleventyConfig.watchIgnores.add('**/.cache/**')

  // i18n plugin
  eleventyConfig.addPlugin(EleventyI18nPlugin, {
    // any valid BCP 47-compatible language tag is supported
    defaultLanguage: 'en',
    errorMode: 'allow-fallback',
  })

  const widths = [300, 770, 1200, 2400]
  const formats = ['svg', 'webp', 'auto']
  eleventyConfig.addPlugin(eleventyImageTransformPlugin, {
    // @see https://www.11ty.dev/docs/plugins/image/
    extensions: 'html',
    urlPath: `/img/`,
    formats,
    widths,
    svgShortCircuit: true,
    defaultAttributes: {
      loading: 'lazy',
      decoding: 'async',
      sizes: ['100vw'],
      alt: '',
    },
  })
  return {
    dir: {
      data: '../_data',
    }
  }
}
